<?php

namespace App\Tests;

use DateTime;
use App\Entity\User;
use App\Entity\Blogpost;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $blogpost = new Blogpost();
        $datetime = new DateTime();
        
        $blogpost->setTitre('true@test.com');
        $blogpost->setContenu('Contenu');
        $blogpost->setSlug('Slug');
        $blogpost->setCreatedAt($datetime);
       
        $this->assertTrue( $blogpost->getTitre() === 'true@test.com');
        $this->assertTrue( $blogpost->getContenu() === 'Contenu');
        $this->assertTrue( $blogpost->getSlug() === 'Slug');
        $this->assertTrue( $blogpost->getCreatedAt() === $datetime);
    }

    public function testIsFalse()
    {
        $blogpost = new Blogpost();
        $datetime = new DateTime();
        
        $blogpost->setTitre('true@test.com');
        $blogpost->setContenu('Contenu');
        $blogpost->setSlug('Slug');
        $blogpost->setCreatedAt($datetime);

        $this->assertFalse( $blogpost->getTitre() === 'false@test.com');
        $this->assertFalse( $blogpost->getContenu() === 'false');
        $this->assertFalse( $blogpost->getSlug() === 'false');
        $this->assertFalse( $blogpost->getCreatedAt() === new DateTime());
    }

    public function testIsEmpty()
    {
        $blogpost = new Blogpost();

        $this->assertEmpty($blogpost->getTitre());
        $this->assertEmpty($blogpost->getContenu());
        $this->assertEmpty($blogpost->getSlug());
        $this->assertEmpty($blogpost->getCreatedAt());
    }
}

