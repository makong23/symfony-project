<?php

namespace App\Tests;

use DateTime;
use App\Entity\Peinture;
use App\Entity\Blogpost;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $commentaire = new Commentaire();
        $peinture = new Peinture();
        $blogpost = new Blogpost();
        $datetime = new DateTime();
        
        $commentaire->setAuteur('true@test.com');
        $commentaire->setContenu('Contenu');
        $commentaire->setEmail('email');
       // $commentaire->setCreatedAt($datetime);
        $commentaire->setPeinture($peinture);
        $commentaire->setBlogpost($blogpost);
       
        $this->assertTrue( $commentaire->getAuteur() === 'true@test.com');
        $this->assertTrue( $commentaire->getContenu() === 'Contenu');
        $this->assertTrue( $commentaire->getEmail() === 'email');
       // $this->assertTrue( $commentaire->getCreateAt() === $datetime);
        $this->assertTrue( $commentaire->getPeinture() === $peinture);
        $this->assertTrue( $commentaire->getBlogpost() === $blogpost);
    }

    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $peinture = new Peinture();
        $blogpost = new Blogpost();
        $datetime = new DateTime();
        
        $commentaire->setAuteur('true@test.com');
        $commentaire->setContenu('Contenu');
        $commentaire->setemail('email');
        //$commentaire->setCreatedAt(new DateTime());
        $commentaire->setPeinture(new Peinture());
        $commentaire->setBlogpost(new Blogpost());

        $this->assertFalse( $commentaire->getAuteur() === 'false@test.com');
        $this->assertFalse( $commentaire->getContenu() === 'false');
        $this->assertFalse( $commentaire->getEmail() === 'false');
        //$this->assertFalse( $commentaire->getCreatedAt() === $datetime);
        $this->assertFalse( $commentaire->getPeinture() === $peinture);
        $this->assertFalse( $commentaire->getBlogpost() === $blogpost);
    }

    public function testIsEmpty()
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getEmail());
        //$this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getBlogpost());
    }

}
