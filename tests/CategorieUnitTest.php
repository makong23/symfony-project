<?php

namespace App\Tests;

use App\Entity\Categorie;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $categorie = new Categorie();
        
        $categorie->setNom('true');
        $categorie->setDescription('prenom');
        $categorie->setSlug('nom');

        $this->assertTrue( $categorie->getNom() === 'true');
        $this->assertTrue( $categorie->getDescription() === 'prenom');
        $this->assertTrue( $categorie->getSlug() === 'nom');
    }

    public function testIsFalse()
    {
        $categorie = new Categorie();
        
        $categorie->setNom('true@test.com');
        $categorie->setDescription('prenom');
        $categorie->setSlug('nom');

        $this->assertFalse( $categorie->getNom() === 'false@test.com');
        $this->assertFalse( $categorie->getDescription() === 'false');
        $this->assertFalse( $categorie->getSlug() === 'false');
    }

    public function testIsEmpty()
    {
        $categorie = new Categorie();

        $this->assertEmpty($categorie->getNom());
        $this->assertEmpty($categorie->getDescription());
        $this->assertEmpty($categorie->getSlug());
    }
}